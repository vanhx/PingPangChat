<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="${httpServletRequest.getContextPath()}/css/layui.css"  media="all">
  <link rel="stylesheet" href="${httpServletRequest.getContextPath()}/x-admin/css/login.css"  media="all">
  <link rel="stylesheet" href="${httpServletRequest.getContextPath()}/css/style.css">
  <title>乒乓聊天</title> 
  <script src="${httpServletRequest.getContextPath()}/audio/HZRecorder.js"></script>
  <script src="${httpServletRequest.getContextPath()}/jquery.min.js"></script>
  <script src="${httpServletRequest.getContextPath()}/layer.js"></script>
  <script src="${httpServletRequest.getContextPath()}/layui.js"></script>
  <script type="text/javascript" src="${httpServletRequest.getContextPath()}/x-admin/js/xadmin.js"></script>
  
  <style type="text/css">
   
         .sender{
            clear:both;
        }
        .sender div:nth-of-type(1){
            float: left;
        }
        .sender div:nth-of-type(2){
            background-color: aquamarine;
            float: left;
            margin: 0 20px 10px 15px;
            padding: 10px 10px 10px 0px;
            border-radius:7px;
        }

        .receiver div:first-child img,
        .sender div:first-child img{
            width:50px;
            height: 50px;
        }

        .receiver{
            clear:both;
        }
        .receiver div:nth-child(1){
            float: right;
        }
        .receiver div:nth-of-type(2){
            float:right;
            background-color: gold;
            margin: 0 10px 10px 20px;
            padding: 10px 0px 10px 10px;
            border-radius:7px;
        }

        .left_triangle{
            height:0px;  
            width:0px;  
            border-width:8px;  
            border-style:solid;  
            border-color:transparent aquamarine transparent transparent;  
            position: relative;
            left:-16px;
            top:3px;
        }

        .right_triangle{
            height:0px;  
            width:0px;  
            border-width:8px;  
            border-style:solid;  
            border-color:transparent transparent transparent gold;  
            position: relative;
            right:-16px;
            top:3px;
        }
        
        .divcss5-b{ margin-left:10px;height:340px;hoverflow-y:scroll; overflow-x:scroll;} 
        
        .myskin{
             background-color:transparent;//透明（可根据需求自己调整）
             opacity: 0.3;//透明度
        }
        
        /* .divcss5-b{ margin-left:10px;width: 100%;height:58%;hoverflow-y:scroll; overflow-x:scroll;} */ 
    </style>
</head>
<body class="login-bg" >
 <img alt="" style="display:none;" id="displayImg" src="" height="500" width="500"/>
 <!-- <audio controls autoplay></audio> -->
<script>
layui.use('element', function(){
  var $ = layui.jquery
  ,element = layui.element; //Tab的切换功能，切换事件监听等，需要依赖element模块
  
  //触发事件
  var active = {
    tabAdd: function(){
      //新增一个Tab项
      element.tabAdd('demo', {
        title: '新选项'+ (Math.random()*1000|0) //用于演示
        ,content: '内容'+ (Math.random()*1000|0)
        ,id: new Date().getTime() //实际使用一般是规定好的id，这里以时间戳模拟下
      })
    }
    ,tabDelete: function(othis){
      //删除指定Tab项
      element.tabDelete('demo', '44'); //删除：“商品管理”
      
      
      othis.addClass('layui-btn-disabled');
    }
    ,tabChange: function(){
      //切换到指定Tab项
      element.tabChange('demo', '22'); //切换到：用户管理
    }
  };
  
  $('.site-demo-active').on('click', function(){
    var othis = $(this), type = othis.data('type');
    active[type] ? active[type].call(this, othis) : '';
  });
  
  //Hash地址的定位
  var layid = location.hash.replace(/^#test=/, '');
  element.tabChange('test', layid);
  
  element.on('tab(test)', function(elem){
    location.hash = 'test='+ $(this).attr('lay-id');
  });
  
});
</script>

<script>
//用户编号
var userCode="${userCode}";
var userName="${userName}";
var layedit;
var indexMap=new Map();

//这里保存没有打开对话框的数据
var alertChatMsg=new Set();
var alertChatCount=0;

;!function(){
//页面一打开就执行，放入ready是为了layer所需配件（css、扩展模块）加载完毕
layer.ready(function(){ 
  layer.open({
    type: 1,
    shade: 0,
    title: ['${userName}','color:#fff;background-color:#01AAED;'],
    maxmin: true,
	closeBtn: 0,
    area: ['300px', '500px'],
	offset: ['60px', '200px'],
	cancel : function(){
				              // 你点击右上角 X 取消后要做什么
					},
    content: 
'<div>&nbsp;&nbsp;<a href="${httpServletRequest.getContextPath()}/user/logOut">退出</a>'+
      '&nbsp;&nbsp;<a  href="javascript:void(0)" onclick="addGroup(this)">建群</a>'+
'</div>'+
'<div class="layui-tab layui-tab-card site-demo-button">'+
'<ul class="layui-tab-title">'+
   '<li class="layui-this">在线</li>'+
    '<li>群组</li>'+
    '<li>最近聊天</li>'+
  '</ul>'+
  '<div class="layui-tab-content">'+
    '<div class="layui-tab-item layui-show">'+
        //'<dl>'+
        '<ul id="user_list" class="people">'+
          /*
          '<dd><a onclick="chatSingle(\'s003\',\'3\')">张三</a></dd>'+
          '<dd><a onclick="chatSingle(\'s004\',\'3\')">李四</a></dd>'+
          '<dd><a onclick="chatSingle(\'s005\',\'3\')">王五</a></dd>'+
          */
         '</ul>'+  
        //'</dl>'+
    '</div>'+
    '<div class="layui-tab-item">'+
    '<ul id="group_user_list" class="people">'+
      //'<li class="person" onclick="chatSingle(\'划水(g001)\',\'g001\',\'4\')"><span>划水(g001)</span>&nbsp;&nbsp;<i class="layui-icon">&#xe624;</i>&nbsp;&nbsp;<i class="layui-icon">&#xe67e;</i></li>'+
      //'<li class="person" onclick="chatSingle(\'闲聊(g002)\',\'g002\',\'4\')"><span>闲聊(g002)</span></li>'+
      //'<li class="person" onclick="chatSingle(\'讨论(g003)\',\'g003\',\'4\')"><span>讨论(g003)</span></li>'+
    '</ul>'+
	'</div>'+
    '<div class="layui-tab-item">'+
    '<ul id="old_user_list" class="people">'+
    '</ul>'+
    '</div>'+
  '</div>'+
'</div>'
  });
});
}();

function chatSingle(toUserName,toUserCode,cmd){
	
	//群组进来先走绑定
 if("4"==cmd){
	      getAllUser("7",toUserCode);
 }	
 
 if($("#chat"+toUserCode).length>0){
  return false;
 };

 layer.open({
    type: 1,
    title: [userName+"---->"+toUserName,'color:#fff;background-color:#01AAED;'],
    maxmin: true,
    area: ['700px', '650px'],
	offset: ['60px', '600px'],
	shade: 0,
    content: '<div class="divcss5-b">'+
             '<div class="right">'+
             '<div id="chat'+toUserCode+'" class="chat" data-chat="'+toUserCode+'">'+
             '</div></div></div>'+
	           '<div style="margin-bottom: 20px; width: 500px;">'+
                    '<textarea class="layui-textarea" id="LAY_demo'+toUserCode+'"></textarea>'+
               '</div>'+
               '<div style="margin-bottom: 5px; width: 500px;">'+
                    '<audio id="audio'+toUserCode+'" controls autoplay></audio>'+
               '</div>'+
                  '<button class="layui-btn" lay-submit lay-filter="formDemo" onclick="send(\''+toUserCode+'\',\''+cmd+'\');">发送</button>'+
                  '<button type="button" class="layui-btn layui-btn-sm layui-btn-normal" id="btnStart" onclick="startRecording(\''+toUserCode+'\')"><i class="layui-icon">&#xe6fc;</i>录制</button>'+
                  '<button type="button" class="layui-btn layui-btn-sm layui-btn-disabled"  id="btnStop" onclick="stopRecording(\''+toUserCode+'\')" > <i class="layui-icon">&#xe651;</i>停止</button>'+
                  '<button type="button" class="layui-btn layui-btn-sm layui-btn-disabled" id="btnPlay" onclick="playRecording(\''+toUserCode+'\')" disabled="disabled"><i class="layui-icon">&#xe652;</i>播放</button>'+
                  '<button type="button" class="layui-btn layui-btn-sm layui-btn-disabled" id="btnUpload" onclick="uploadAudio(\''+toUserCode+'\')" disabled="disabled"><i class="layui-icon">&#xe681;</i>上传音频</button>'
	});
 
 layui.use('layedit', function(){
	 if(null==layedit){
	  layedit = layui.layedit;
	 }
	 
	 layedit.set({
		                uploadImage: {
		                     url: '${httpServletRequest.getContextPath()}/fileUpload/imageUpload' //接口url
		                    ,type: 'post' //默认post
		                },
						uploadVideo: {
							 url: '${httpServletRequest.getContextPath()}/fileUpload/imageUpload', //接口url
					         accept: 'video',
					         acceptMime: 'video/*',
					         exts: 'mp4|flv|avi|rm|rmvb'
					     },
					     uploadAudio: {
					    	 url: '${httpServletRequest.getContextPath()}/fileUpload/imageUpload', //接口url,
					         accept: 'audio',
					         acceptMime: 'audio/*',
					         exts: 'mp3|m4a|wma|amr|ape|flac|aac'
					     }
		            });
	 
     //var index;
	  //,$ = layui.jquery;
	  //自定义工具栏
	 var index=layedit.build('LAY_demo'+toUserCode, {
	    tool: ['face', 'left', 'center', 'right','image','video', 'audio']
	    ,height: 100
	  });
     indexMap.set(toUserCode,index);
	});	
 
 if("2"==cmd){
	 //console.log("下线提示!");
	 //document.querySelector('.chat[data-chat=person1]').classList.add('active-chat');
	 return;
 }
 
 $.ajax({
     url:"${httpServletRequest.getContextPath()}/msg/getUserOldMsg",
     data:{'cmd':cmd,'from.userCode':userCode,'accept.userCode':toUserCode,'group.groupCode':toUserCode},
     type:"Post",
     dataType:"json",
     success:function(data){
     	//var oldUser=eval("('"+data+"')");
     	var oldUser=data;
     	//console.log(oldUser);
     	//alert(oldUser.CODE);
         if("S"==oldUser.CODE){
        	 for(var message of oldUser.userOldMsg){
        		   if("3"==message.cmd){
					  if(userCode!=message.from.userCode){
						  $("#chat"+message.from.userCode).append('<div class="conversation-start"><span>'+message.createDate+'</span></div>');
						  $("#chat"+message.from.userCode).append("<div class=\"sender\"><div>"+message.from.userName+"("+message.from.userCode+")</div></div><div class=\"bubble you\">"+message.msg+"</div>");
					  }else if(userCode==message.from.userCode){
						  $("#chat"+message.accept.userCode).append('<div class="conversation-start"><span>'+message.createDate+'</span></div>');
						  $("#chat"+message.accept.userCode).append("<div class=\"receiver\"><div>"+message.from.userName+"("+message.from.userCode+")</div></div><div class=\"bubble me\">"+message.msg+"</div>");
					  }
					      document.querySelector('.chat[data-chat="'+toUserCode+'"]').classList.add('active-chat');
					}else if("4"==message.cmd){
						if(userCode!=message.from.userCode){
							  $("#chat"+message.group.groupCode).append('<div class="conversation-start"><span>'+message.createDate+'</span></div>');
							  $("#chat"+message.group.groupCode).append("<div class=\"sender\"><div>"+message.from.userName+"("+message.from.userCode+")</div></div><div class=\"bubble you\">"+message.msg+"</div>");
						  }else if(userCode==message.from.userCode){
							  $("#chat"+message.group.groupCode).append('<div class="conversation-start"><span>'+message.createDate+'</span></div>');
							  $("#chat"+message.group.groupCode).append("<div class=\"receiver\"><div>"+message.from.userName+"("+message.from.userCode+")</div></div><div class=\"bubble me\">"+message.msg+"</div>");
						  }
						  document.querySelector('.chat[data-chat="'+toUserCode+'"]').classList.add('active-chat');
				   } 
			   }
         }
     },
     error:function(data){
         $.messager.alert('错误',data.msg);
     }
 });
};

function send(name,cmd){
  //console.log(name+" "+cmd);
  //console.log(layedit.getContent(indexMap.get(name)));
  //console.log(layedit.getText(indexMap.get(name)));
  //var text=$('.layui-textarea').val();
  var text=layedit.getContent(indexMap.get(name));
  if(text.length == 0){
	  return;
  }
    $("#chat"+name).append("<div class=\"receiver\"><div>"+userName+"("+userCode+")</div></div><div class=\"bubble me\">"+text+"</div>");
	document.querySelector('.chat[data-chat="'+name+'"]').classList.add('active-chat');
	layedit.setContent(indexMap.get(name),"");
	var bindMsg = {};
	var from ={};
	var accept={};
    from.userCode=userCode;
    bindMsg.cmd=cmd;
    bindMsg.from=from;
    
    if("3"==cmd){
    	accept.userCode=name;
    	bindMsg.accept=accept;
    }else if("4"==cmd){
    	var group={};
    	bindMsg.group=group;
    	group.groupCode=name;
    }
	bindMsg.msg=text;
    ws.send(JSON.stringify(bindMsg));
};

//HTML转义
function HTMLEncode(html) {
var temp = document.createElement("div");
(temp.textContent != null) ? (temp.textContent = html) : (temp.innerText = html);
var output = temp.innerHTML;
temp = null;
return output;
};

var ws = new WebSocket("wss://${chatPath}:8089/ws");
ws.onopen = function(){  
   //console.log("open");
   //初始化绑定信息
   var bindMsg = {};
   var from={};
   
   bindMsg.cmd="1";
   from.userCode=userCode;
   from.userName=userName;
   bindMsg.from=from;
   //console.log("登录");
   ws.send(JSON.stringify(bindMsg));
};

ws.onmessage = function(evt){
  //console.log(evt.data);
  if("pong"==evt.data){
	  return;
  }else if("Heartbeat"==evt.data){
   ws.send("Heartbeat");
	return;  
  }
  
  //console.log(eval("("+evt.data+")"));
  var message=eval("("+evt.data+")");
  if("2"==message.cmd){
	  if($("#chat"+message.from.userCode).length<=0){
		  alertChatMsg.add(message);
		  alertChatCount=alertChatMsg.size;
	      alertMsg();
		  return false;
	  }
	  $("#chat"+message.from.userCode).append('<div class="conversation-start"><span>'+message.createDate+'</span></div>');
	  $("#chat"+message.from.userCode).append("<div class=\"sender\"><div>"+message.from.userName+"("+message.from.userCode+")</div></div><div class=\"bubble you\">"+message.msg+"</div>");
	  document.querySelector('.chat[data-chat="'+message.from.userCode+'"]').classList.add('active-chat');
	  
   }else if("3"==message.cmd){
	  if($("#chat"+message.from.userCode).length<=0){
		  alertChatMsg.add(message);
		  alertChatCount=alertChatMsg.size;
	      alertMsg();
		  return false;
	  }
	  $("#chat"+message.from.userCode).append('<div class="conversation-start"><span>'+message.createDate+'</span></div>');
	  $("#chat"+message.from.userCode).append("<div class=\"sender\"><div>"+message.from.userName+"("+message.from.userCode+")</div></div><div class=\"bubble you\">"+message.msg+"</div>");
	  document.querySelector('.chat[data-chat="'+message.from.userCode+'"]').classList.add('active-chat');
  }else if("4"==message.cmd){
	  if($("#chat"+message.group.groupCode).length<=0){
		  alertChatMsg.add(message);
		  alertChatCount=alertChatMsg.size;
	      alertMsg();
		  return false;
	  }
	  $("#chat"+message.group.groupCode).append('<div class="conversation-start"><span>'+message.createDate+'</span></div>');
	  $("#chat"+message.group.groupCode).append("<div class=\"sender\"><div>"+message.from.userName+"("+message.from.userCode+")</div></div><div class=\"bubble you\">"+message.msg+"</div>");
	  document.querySelector('.chat[data-chat="'+message.group.groupCode+'"]').classList.add('active-chat'); 
  }else if("5"==message.cmd){
	  var currentUser=message.chatSet;
	  $("#user_list").html("");
	  for(var i = 0; i < currentUser.length; i++){
	     //console.log(currentUser[i].userCode + " " + currentUser[i].userName);
	     //$("#user_list").append('<dd><a onclick="chatSingle(\''+currentUser[i].userName+'\',\''+currentUser[i].userCode+'\',\'3\')">'+currentUser[i].userName+'('+currentUser[i].userCode+')</a></dd>');
         $("#user_list").append('<li class="person" onclick="chatSingle(\''+currentUser[i].userName+'\',\''+currentUser[i].userCode+'\',\'3\')"><span>'+currentUser[i].userName+'('+currentUser[i].userCode+')</span></li>');
	  }
  }else if("6"==message.cmd){
	  var currentUser=message.groupSet;
	  $("#group_user_list").html("");
	  for(var i = 0; i < currentUser.length; i++){
		 //'<li class="person" onclick="chatSingle(\'划水(g001)\',\'g001\',\'4\')"><span>划水(g001)</span>&nbsp;&nbsp;<i class="layui-icon">&#xe624;</i>&nbsp;&nbsp;<i class="layui-icon">&#xe67e;</i></li>'+
         $("#group_user_list").append('<li class="person" ><span>'+currentUser[i].groupName+'('+currentUser[i].groupCode+')</span><span style="position:absolute; right:0px;"><i class="layui-icon" onclick="chatSingle(\''+currentUser[i].groupName+'\',\''+currentUser[i].groupCode+'\',\'4\')">&#xe624;</i>&nbsp;&nbsp;<i class="layui-icon" onclick="getAllUser(\'8\',\''+currentUser[i].groupCode+'\')">&#xe67e;</i>&nbsp;&nbsp;<span class="layui-bg-red">'+currentUser[i].groupUserCount+'</span></span></li>');
	  }
  }
};

ws.onclose = function(evt){
  console.log("WebSocketClosed!");
};

ws.onerror = function(evt){
  console.log("WebSocketError!");
};

//---------------------------
//5.获取用户信息 6获取群组用户信息
function getAllUser(cmd,groupCode){
	 var bindMsg = {};
	 var from={};
	 bindMsg.cmd=cmd;
	 from.userCode=userCode;
	 bindMsg.from=from;
	 
     if("7"==cmd || "8"==cmd){
    	 var group={};
    	 group.groupCode=groupCode;
    	 bindMsg.group=group;
     }	 
	 ws.send(JSON.stringify(bindMsg));
}
//setInterval(getAllUser ,5000); 
setInterval(function(){getAllUser(5,"");},5000);
setInterval(function(){getAllUser(6,"");},5000);
//---------------------------

function test(){ 
	ws.send("ping");
	//console.log("ping");
} 
setInterval(test ,10000); 

function alertMsg(){
	 var index=layer.open({
		  type: 0,
		  shade: 0,
		  title: '消息提醒',
		  content: '你有'+alertChatCount+"条消息未接受!",
		  offset: 'rb',
		  yes: function(){
			   for(var message of alertChatMsg){
				   if("2"==message.cmd){
					   //alert(2);
				       chatSingle(message.from.userName,message.from.userCode,message.cmd);
				       $("#chat"+message.from.userCode).append('<div class="conversation-start"><span>'+message.createDate+'</span></div>');
				 	   $("#chat"+message.from.userCode).append("<div class=\"sender\"><div>"+message.from.userName+"("+message.from.userCode+")</div></div><div class=\"bubble you\">"+message.msg+"</div>");
				 	   document.querySelector('.chat[data-chat="'+message.from.userCode+'"]').classList.add('active-chat');
				       //$("#chat"+message.from.userCode).append("<div class=\"sender\"><div>"+message.from.userName+"("+message.from.userCode+")</div><div><div class=\"left_triangle\"></div><span>"+message.msg+"</span></div>");
				   }else if("3"==message.cmd){
				       chatSingle(message.from.userName,message.from.userCode,message.cmd);
					   //$("#chat"+message.from.userCode).append("<div class=\"sender\"><div>"+message.from.userName+"("+message.from.userCode+")</div><div><div class=\"left_triangle\"></div><span>"+message.msg+"</span></div>");
				   }else if("4"==message.cmd){
					   chatSingle(message.group.groupName+"("+message.group.groupCode+")",message.group.groupCode,message.cmd);
					   //$("#chat"+message.group).append("<div class=\"sender\"><div>"+message.from.userName+"("+message.from.userCode+")</div><div><div class=\"left_triangle\"></div><span>"+message.msg+"</span></div>");
				   }
			   }
			   alertChatMsg.clear();
			   parent.layer.close(index); //再执行关闭
	      }
	   });
}

$(document).on('click', '#im_img', function() {
	        var url = $(this).attr('src').replace('_2', '');
	        //console.log("图片原图："+url);
	        $("#displayImg").attr("src", url);
	        var height = $("#displayImg").height();
	        var width = $("#displayImg").width();
	        layer.open({
	           type: 1,
	           title: false,
	           closeBtn: 0,//隐藏关闭按钮
	           shade: [0.3, '#000'],//黑色背景（0.3代表颜色深度）
	           shadeClose: true,//点击遮罩关闭大图
	           area: [width + 'px', height + 'px'], //宽高
	           resize:false,//不可拖拽缩放
	           skin: 'myskin',//大图背景色定义类
	           content: "<img  src=" + url + " height=500px; width=500px;" + "/>"
	        });
	    })

function addGroup(addGroup){
	layer.open({
		           type: 2,
		           title: false,
		           //closeBtn: 0,//隐藏关闭按钮
		           shade: [0.3, '#000'],//黑色背景（0.3代表颜色深度）
		           shadeClose: true,//点击遮罩关闭大图
		           area: ['700px', '650px'],
		           offset: ['60px', '600px'],
		           resize:false,//不可拖拽缩放
		           skin: 'myskin',//大图背景色定义类
		           content: '${httpServletRequest.getContextPath()}/group/chat-add-group'
		        });
}
</script>

<!-- 获取音频信息 -->
<script>

        var recorder;

        var audio;

        //录音
        function startRecording(toUserCode) {
        	 //layedit.setContent(indexMap.get(toUserCode),"<audio id=\"audio123\" controls autoplay></audio>");
        	 //audio = document.querySelector('#audio123');
        	//alert(userCode);
        	//layedit.setContent("LAY_demo"+toUserCode,"<audio controls autoplay></audio>");
        	audio=document.querySelector('#audio'+toUserCode);
        	//console();
        	$('#audio'+toUserCode).attr('src',"");
            //console.log('开始录音')
            document.getElementById('btnStop').className = 'layui-btn layui-btn-sm';
            document.getElementById('btnStop').disabled = "";
            document.getElementById('btnUpload').className = 'layui-btn layui-btn-sm';
            document.getElementById('btnUpload').disabled = "";
            document.getElementById('btnPlay').className = 'layui-btn layui-btn-sm';
            document.getElementById('btnPlay').disabled = "";
            document.getElementById('btnStart').disabled = 'disabled';
            document.getElementById('btnStart').className = 'layui-btn layui-btn-sm layui-btn-disabled';
            HZRecorder.get(function (rec) {
                recorder = rec;
                recorder.start();
            }, {
                    sampleBits: 16,
                    sampleRate: 16000
                });
        }
         //停止录音
        function stopRecording(toUserCode) {
            recorder.stop();
            document.getElementById('btnStart').disabled = '';
            document.getElementById('btnStart').className = 'layui-btn layui-btn-sm layui-btn-normal';
        }
         //播放
        function playRecording(toUserCode) {
        	//audio=document.querySelector('#audio123');
            recorder.play(audio);
            document.getElementById('btnStart').disabled = '';
            document.getElementById('btnStart').className = 'layui-btn layui-btn-sm layui-btn-normal';
            document.getElementById('btnStop').className = 'layui-btn layui-btn-sm layui-btn-disabled';
            document.getElementById('btnStop').disabled = "disabled";
        }
         //上传录音文件
        function uploadAudio(toUserCode) {
            recorder.upload("${httpServletRequest.getContextPath()}/fileUpload/audioUpload", function (state) {
            	//alert(state);
            	var message=eval("("+state+")");
            	//console.log(message);
                if("0"==message.code){
                	layedit.setContent(indexMap.get(toUserCode),"<audio controls  src=\""+message.data.src+"\"></audio>");
                }else{
                	layer.alert('上传失败!'); 
                }
            });
        }

    </script>

<!--获取用户列表  -->    
<script type="text/javascript">
function getOldUserCode(){
	$.ajax({
        url:"${httpServletRequest.getContextPath()}/msg/getUserOldChat",
        data:{'userCode':userCode},
        type:"Post",
        dataType:"json",
        success:function(data){
        	var oldUser=data;
            if("S"==oldUser.CODE){
            	$("#old_user_list").html("");
            	for(var i in data.userList){
                    //$("#old_user_list").append('<dd><a onclick="chatSingle(\''+oldUser.userList[i].USER_NAME+'\',\''+oldUser.userList[i].USER_CODE+'\',\'3\')">'+oldUser.userList[i].USER_NAME+'('+oldUser.userList[i].USER_CODE+')</a></dd>');
                    $("#old_user_list").append('<li class="person" onclick="chatSingle(\''+oldUser.userList[i].USER_NAME+'\',\''+oldUser.userList[i].USER_CODE+'\',\'3\')"><span>'+oldUser.userList[i].USER_NAME+'('+oldUser.userList[i].USER_CODE+')</span></li>');
            	   }
                }
        },
        error:function(data){
            $.messager.alert('错误',data.msg);
        }
    });
}
setInterval(getOldUserCode ,5000); 
</script>
    
</body>
</html>