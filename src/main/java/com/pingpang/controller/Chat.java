package com.pingpang.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.pingpang.service.UserService;
import com.pingpang.util.StringUtil;
import com.pingpang.websocketchat.ChannelManager;
import com.pingpang.websocketchat.ChartUser;

@Configuration
@RestController
@RequestMapping("/user")
public class Chat {
	
	@Value("${adminUserName}")
	private String adminUserName;
	
	@Value("${adminPassWord}")  
	private String adminPassWord;
	
	@Autowired
	private UserService userService;
	
	/**
	  * 主页
	 * @return
	 */
	@RequestMapping("/index")
	public ModelAndView index() {
		return new ModelAndView("index");
	}

	/**
	  * 用户登录
	 * @param userName
	 * @param userCode
	 * @return
	 */
	@RequestMapping("/chat")
	public ModelAndView chat(ChartUser cu) {
		if (null == cu.getUserPassword() ||"".equals(cu.getUserPassword()) ||null == cu.getUserCode() || "".equals(cu.getUserCode())) {
			return new ModelAndView("index");
		}

		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		//String scheme = request.getScheme();// http
		String serverName = request.getServerName();// localhost
		//int serverPort = request.getServerPort();// 8080
		//String contextPath = request.getContextPath();// 项目名
		//String url = scheme + "://" + serverName + ":" + serverPort + contextPath;// http://127.0.0.1:8080/test
		//String url = serverName;
		ModelAndView mav = new ModelAndView();
		/*
		 * if(null != ChannelManager.getChartUser(userCode)) { mav.setViewName("index");
		 * mav.addObject("errorMsg", "用户编码已存在，请换个编码!"); return mav; }
		 */	
		//管理员直接进入到管理账户页面
		if(cu.getUserCode().equals(this.adminUserName) && cu.getUserPassword().equals(this.adminPassWord)) {
			mav.setViewName("adminView");
			//这里是管理员页面
			request.getSession().setAttribute("isAdmin", true);
			request.getSession().setAttribute("user", cu);
			return mav;
		}
		
        cu=this.userService.getUser(cu);
        if(null==cu) {
        	request.setAttribute("errorMsg", "用户名或密码不正确!");
        	return new ModelAndView("index");
        }
        
        //用户状态-1:注销,0:离线,1:在线
        if("-1".equals(cu.getUserStatus())) {
        	request.setAttribute("errorMsg", "用户注销请联系管理员!");
        	return new ModelAndView("index");
        }
        
        request.getSession().setAttribute("user", cu);
        
        //把数据改为登录状态
        userService.dbDownUser(cu.getUserCode(), "1");
        
        mav.setViewName("chat");
		mav.addObject("userName", cu.getUserName());
		mav.addObject("userCode", cu.getUserCode());
		mav.addObject("chatPath", serverName);
		return mav;
	}
	
	/**
	   * 用户退出
	 */
	@RequestMapping("/logOut")
	public ModelAndView index(HttpServletRequest request) {
		ChartUser cu=(ChartUser) request.getSession().getAttribute("user");
		
		//移除session
		request.getSession().removeAttribute("user");
		//移除聊天信息
		if(!StringUtil.isNUll(cu.getUserCode())) {
			ChannelManager.removeChannelByCode(cu.getUserCode());

			//把数据改为离线状态
			userService.dbDownUser(cu.getUserCode(), "0");
		}
		
        
		return new ModelAndView("index");
	}
	
	
	/**
	   * 测试代码
	 * @return
	 */
	@RequestMapping("/audio")
	public ModelAndView audio() {
		return new ModelAndView("audio");
	}
}
