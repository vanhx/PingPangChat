package com.pingpang.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class ActuatorWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		    web.ignoring().antMatchers("/user/**","/msg/**","/fileUpload/**","/userController/**","/group/**");
	}

	@Override
    protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		  .antMatchers("/actuator/**")
	      .hasRole("ENDPOINT_ADMIN")
	      .and()
	      .formLogin()
	      .and()
	      .httpBasic();
    }
}