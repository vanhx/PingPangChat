package com.pingpang.websocketchat.send.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.pingpang.websocketchat.ChannelManager;
import com.pingpang.websocketchat.Message;
import com.pingpang.websocketchat.send.ChatSend;

import io.netty.channel.ChannelHandlerContext;

/**
 * 离线操作
 * @author dell
 */
public class ChatSendLeave extends ChatSend{

	@Override
	public void send(Message message, ChannelHandlerContext ctx) throws JsonProcessingException {
		ChannelManager.removeChannelByCode(message.getFrom().getUserCode());
	}
}
