package com.pingpang.websocketchat.send.impl;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.pingpang.websocketchat.ChannelManager;
import com.pingpang.websocketchat.ChatGroup;
import com.pingpang.websocketchat.Message;
import com.pingpang.websocketchat.send.ChatSend;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

/**
 * 获取群组信息
 * @author dell
 */
public class ChatSendQueryGroup extends ChatSend{

	@Override
	public void send(Message message, ChannelHandlerContext ctx) throws JsonProcessingException {
		Map<String,String> queryMap=new HashMap<String,String>();
		queryMap.put("groupStatus", "0");
		message.setGroupSet(this.userGroupService.getAllGroupSet(queryMap));
        for(ChatGroup cg : message.getGroupSet()) {
        	if(null!=ChannelManager.getGroupUser(cg)) {
        		cg.setGroupUserCount(ChannelManager.getGroupUser(cg).size());
        	}
        }		
		ctx.channel().writeAndFlush(new TextWebSocketFrame(this.getMapper().writeValueAsString(message)));
	}
}
