package com.pingpang.websocketchat.send.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.pingpang.util.StringUtil;
import com.pingpang.websocketchat.ChannelManager;
import com.pingpang.websocketchat.Message;
import com.pingpang.websocketchat.send.ChatSend;

import io.netty.channel.ChannelHandlerContext;

public class ChatSendRemoreGroup extends ChatSend{

	@Override
	public void send(Message message, ChannelHandlerContext ctx) throws JsonProcessingException {
		if(StringUtil.isNUll(message.getFrom().getUserCode()) || StringUtil.isNUll(message.getGroup().getGroupCode())) {
			return;
		}
		
		ChannelManager.removeGroup(message.getGroup(), message.getFrom().getUserCode());
	}

}
