package com.pingpang.websocketchat.send.impl;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pingpang.util.StringUtil;
import com.pingpang.websocketchat.ChatType;
import com.pingpang.websocketchat.Message;
import com.pingpang.websocketchat.send.ChatSend;

import io.netty.channel.ChannelHandlerContext;

public class ChatSendUtil {

	//执行类存储
	private static Map<String,ChatSend> currentChat=new ConcurrentHashMap<String,ChatSend>();
    
	//初始化操作
	private static void init() {
		//绑定操作
		currentChat.put(ChatType.BIND, new ChatSendBind());
		
		//离线操作
		currentChat.put(ChatType.LEAVE, new ChatSendLeave());
		
		//单聊
		currentChat.put(ChatType.SINGLE, new ChatSendSingle());
		
		//群聊
		currentChat.put(ChatType.GROUP, new ChatSendGroup());
		
		//查询用户在线用户
		currentChat.put(ChatType.QUERY_USER, new ChatSendQueryUser());
		
		//查询群组信息的
		currentChat.put(ChatType.QUERY_GROUP, new ChatSendQueryGroup());
		
		//进群
	    currentChat.put(ChatType.ADD_GROUP, new ChatSendAddGroup());
				
		//离群
	    currentChat.put(ChatType.REMORE_GROUP, new ChatSendRemoreGroup());
	}
	
	//获取执行类
	public static void getChatSend(String msg, ChannelHandlerContext ctx) throws JsonParseException, JsonMappingException, IOException {
		
		if(StringUtil.isNUll(msg)) {
			return;
		}
		
		if(currentChat.isEmpty()) {
			init();
		}
		
		ObjectMapper mapper = new ObjectMapper();
		Message message = mapper.readValue(msg, Message.class);
		
		if(null!=message &&
		   !StringUtil.isNUll(message.getCmd()) && 
		   null!=currentChat.get(message.getCmd())) {
		   currentChat.get(message.getCmd()).isSend(message, ctx);
		}
	}
	
}
