package com.pingpang.websocketchat.send;

import java.io.IOException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pingpang.config.SpringContextUtil;
import com.pingpang.redis.RedisPre;
import com.pingpang.redis.service.RedisService;
import com.pingpang.service.UserGroupService;
import com.pingpang.service.UserMsgService;
import com.pingpang.service.UserService;
import com.pingpang.util.JsonFilterUtil;
import com.pingpang.util.StringUtil;
import com.pingpang.websocketchat.ChannelManager;
import com.pingpang.websocketchat.ChartUser;
import com.pingpang.websocketchat.ChatType;
import com.pingpang.websocketchat.Message;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

public abstract class ChatSend {

	// 用户信息处理
	protected final UserService userService = SpringContextUtil.getBean(UserService.class);

	// 消息信息处理
	protected final UserMsgService userMsgService = SpringContextUtil.getBean(UserMsgService.class);

	// redis操作
	protected final RedisService redisService = SpringContextUtil.getBean(RedisService.class);

	//群聊信息的获取
	protected final UserGroupService userGroupService = SpringContextUtil.getBean(UserGroupService.class);
	
	// 日志操作
	protected Logger logger = LoggerFactory.getLogger(ChatSend.class);

	// 获取JSON工具
	protected final ObjectMapper getMapper() {
		ObjectMapper mapper = new ObjectMapper();
		JsonFilterUtil.addFilterForMapper(mapper);
		return mapper;
	}

	/**
	 * 数据校验
	 * @param msg
	 * @param ctx
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public boolean isSend(Message message, ChannelHandlerContext ctx)throws JsonParseException, JsonMappingException, IOException {
		
		
		if (null != message) {
			logger.info("当前处理信息:"+message.toString());
			// 用户未登录不允许绑定操作
			if (ChatType.BIND.equals(message.getCmd())) {
				ChartUser cu = (ChartUser) redisService.get(RedisPre.DB_USER + message.getFrom().getUserCode());
				if (null == cu || !"1".equals(cu.getUserStatus())) {
					logger.info("未登录用户不允许绑定:" + message.toString());
					return false;
				}
			} else {
				// 防止非当前用户进行操作
				if (!ctx.channel().equals(ChannelManager.getChannel(message.getFrom().getUserCode()))) {
					logger.info("非当前用户操作:" + message.toString());
					return false;
				}

				// 当用户不是在线状态直接打回
				ChartUser fromUser = ChannelManager.getChartUser(message.getFrom().getUserCode());
				if ((!ChatType.QUERY_USER.equals(message.getCmd()) || !ChatType.QUERY_GROUP.equals(message.getCmd())) && !"1".equals(fromUser.getUserStatus())) {
					Channel cl = ChannelManager.getChannel(message.getFrom().getUserCode());

					// 线上禁言
					message.setMsg("系统消息此用户已禁言");
					ChartUser admin = new ChartUser();
					admin.setUserCode("admin");
					admin.setUserName("管理员");
					message.setFrom(admin);
					message.setCmd("2");
					message.setCreateDate(StringUtil.format(new Date()));
					cl.writeAndFlush(new TextWebSocketFrame(this.getMapper().writeValueAsString(message)));

					return false;
				} else if ((ChatType.QUERY_USER.equals(message.getCmd()) || ChatType.QUERY_GROUP.equals(message.getCmd())) && !"1".equals(fromUser.getUserStatus())) {
					return false;
				}
			}
			
			send(message,ctx);
			
			return true;
		}
		
		logger.info("当前处理信息为空!!!");
		return false;
		
	}

	// 发送消息
	public abstract void send(Message message, ChannelHandlerContext ctx) throws JsonProcessingException;
}
