package com.pingpang.websocketchat;

import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pingpang.config.SpringContextUtil;
import com.pingpang.redis.RedisPre;
import com.pingpang.redis.service.RedisService;
import com.pingpang.service.UserGroupService;
import com.pingpang.service.UserService;
import com.pingpang.util.StringUtil;

import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
 
public class ChannelManager {

	// 用户信息处理
    private final static UserService userService = SpringContextUtil.getBean(UserService.class);

    // redis操作
 	private final static RedisService redisService = SpringContextUtil.getBean(RedisService.class);
	
 	/*
	 * 用户  
	 */
	private static Map<ChartUser, Channel> userChannel = new ConcurrentHashMap <ChartUser, Channel>();

	
	//日志操作
    private static Logger logger = LoggerFactory.getLogger(ChannelManager.class);
	
	/**
	 * 通过用户代码获取用户信息
	 * @param userCode
	 * @return
	 */
	public static ChartUser getChartUser(String userCode) {
		if(!StringUtil.isNUll(userCode)) {
			 ChartUser cu=new ChartUser();
			 cu.setUserCode(userCode);
			 return userService.getUser(cu);
		}else {
		     return null;
		}
	}
	
	
	/*
	 * 添加用户
	 */
	public static void addChannel(ChartUser chartUser, Channel ch) {
		logger.info("添加用户:" + chartUser.getUserCode()+chartUser.getUserName()+ "IP:" + ch.remoteAddress());
		userChannel.put(chartUser, ch);
	}

	/*
	 * 获取用户
	 */
	public static Channel getChannel(String userCode) {
       	
		//用户下线了
		if(null==ChannelManager.getChartUser(userCode)) {
			return null;
		} 
		
		return ChannelManager.userChannel.get(ChannelManager.getChartUser(userCode));
	}

	/*
	 * 移除用户
	 */
	public static void removeChannelByCode(String userCode) {
		logger.info("移除用户:" + userCode);
		if(StringUtil.isNUll(userCode) || null==getChartUser(userCode)) {
			return;
		}
		
		/*
		 * Channel cl=ChannelManager.getChannel(userCode); Message message=new
		 * Message(); //线上禁言 message.setMsg("系统消息此用户已下线"); ChartUser admin=new
		 * ChartUser(); admin.setUserCode("admin"); admin.setUserName("管理员");
		 * message.setFrom(admin); message.setCmd("2");
		 * message.setCreateDate(StringUtil.format(new Date()));
		 * 
		 * ObjectMapper mapper = new ObjectMapper();
		 * 
		 * try { cl.writeAndFlush(new
		 * TextWebSocketFrame(mapper.writeValueAsString(message))); } catch
		 * (JsonProcessingException e) { e.printStackTrace(); }
		 */
		sendAlertMsgByCode(userCode, "系统消息此用户已下线");
		
		userChannel.remove(getChartUser(userCode));
		removeGroup(userCode);
	}

	/*
	 * 移除用户
	 */
	public static void removeChannelByChannel(Channel channel) {
		for (ChartUser key : userChannel.keySet()) {
			if (userChannel.get(key).equals(channel)) {
				logger.info("移除用户:" + key + "IP:" + channel.remoteAddress());
				removeChannelByCode(key.getUserCode());
				removeGroup(key.getUserCode());
				return;
			}
		}
	}

	//获取所有用户信息
	public static Set<ChartUser> getAllUser(){
		return userChannel.keySet();
	}
	
	/*
	 * 添加群组
	 */
	public static void addGroup(ChatGroup groupID, ChartUser cu) {
		
         if(null==groupID || null==cu ||StringUtil.isNUll(groupID.getGroupCode()) || StringUtil.isNUll(cu.getUserCode())) {
        	 return;
         }	
         redisService.addSet(RedisPre.DB_GROUP_SET+groupID.getGroupCode(), cu);
	}

	/**
	 * 移除群组
	 * @param groupID
	 * @param userCode
	 */
	public static void removeGroup(ChatGroup groupID,String userCode) {
		if(null==groupID ||StringUtil.isNUll(groupID.getGroupCode()) || StringUtil.isNUll(userCode)) {
       	 return;
        }	
		redisService.removeSet(RedisPre.DB_GROUP_SET+groupID.getGroupCode(),getChartUser(userCode));
	}
	
	/**
	 * 移除群组
	 * @param userCode
	 */
	public static void removeGroup(String userCode) {
		if(StringUtil.isNUll(userCode)) {
			return;
		}
		
		Set<String> groupSet=redisService.getSetPrex(RedisPre.DB_GROUP_SET);
		if(null==groupSet || groupSet.isEmpty()) {
			return;
		}
		
		for(String str:groupSet) {
		   redisService.removeSet(str,getChartUser(userCode));	
		}
	}
	
	/**
	 * 查找是否存在
	 * @param groupID
	 * @param userCode
	 * @return
	 */
	public static boolean isExit(ChatGroup groupID,String userCode) {
		return redisService.contains(RedisPre.DB_GROUP_SET+groupID.getGroupCode(), getChartUser(userCode));
	}
	
	/**
	 * 获取群组用户
	 * @param groupID
	 * @return
	 */
	public static Set<ChartUser> getGroupUser(ChatGroup groupID){
		return (Set<ChartUser>)(Set<?>)redisService.getSet(RedisPre.DB_GROUP_SET+groupID.getGroupCode());
	}
	
	
	/**
	 * 广播消息
	 * @param userCode
	 */
	public static void sendAlertMsgByCode(String userCode,String msg) {
		logger.info("广播用户:" + userCode);
		if(StringUtil.isNUll(userCode) || null==getChartUser(userCode)) {
			return;
		}
		
		Channel cl=ChannelManager.getChannel(userCode);
		Message message=new Message();
		message.setMsg(msg);
		ChartUser admin=new ChartUser();
		admin.setUserCode("admin");
		admin.setUserName("管理员");
		message.setFrom(admin);
		message.setCmd("2");
		message.setCreateDate(StringUtil.format(new Date()));
		
		ObjectMapper mapper = new ObjectMapper();
	 	
		try {
			cl.writeAndFlush(new TextWebSocketFrame(mapper.writeValueAsString(message)));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) {
		ChartUser cu1=new ChartUser();
		cu1.setUserCode("001");
		
		ChartUser cu2=new ChartUser();
		cu2.setUserCode("001");
		
		ChartUser cu3=new ChartUser();
		cu3.setUserCode("001");
		
		ChatGroup cg=new ChatGroup();
		cg.setGroupCode("G001");
		ChannelManager.addGroup(cg, cu1);
		ChannelManager.addGroup(cg, cu2);
		ChannelManager.addGroup(cg, cu3);
		
		System.out.println(ChannelManager.getGroupUser(cg).size());
	}
}
