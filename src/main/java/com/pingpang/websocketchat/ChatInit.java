package com.pingpang.websocketchat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class ChatInit implements CommandLineRunner {

	//日志操作
    private Logger logger = LoggerFactory.getLogger(ChatInit.class);
    
    @Override
    public void run(String... args) throws Exception {
        logger.info("The netty start to initialize ...");
        new WebsocketChatServer(8089).run();
    }
}

