package com.pingpang.websocketchat;

import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pingpang.util.StringUtil;

import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
 
public class ChannelManagerOld {

	/*
	 * 用户  
	 */
	private static Map<ChartUser, Channel> userChannel = new ConcurrentHashMap <ChartUser, Channel>();

	/*
	 * 群聊群组
	 */
	private static Map<ChatGroup, Set<ChartUser>> groupMap = new ConcurrentHashMap <ChatGroup, Set<ChartUser>>();

	
	//日志操作
    private static Logger logger = LoggerFactory.getLogger(ChannelManager.class);
	
	/**
	 * 通过用户代码获取用户信息
	 * @param userCode
	 * @return
	 */
	public static ChartUser getChartUser(String userCode) {
		if(!userChannel.isEmpty()) {
			for(ChartUser cu : userChannel.keySet()) {
				if(cu.getUserCode().equals(userCode)) {
					return cu;
				}
			}
		}
		return null;
	}
	
	
	/*
	 * 添加用户
	 */
	public static void addChannel(ChartUser chartUser, Channel ch) {
		logger.info("添加用户:" + chartUser.getUserCode()+chartUser.getUserName()+ "IP:" + ch.remoteAddress());
		userChannel.put(chartUser, ch);
	}

	/*
	 * 获取用户
	 */
	public static Channel getChannel(String userCode) {
       	
		//用户下线了
		if(null==ChannelManager.getChartUser(userCode)) {
			return null;
		} 
		
		return ChannelManagerOld.userChannel.get(ChannelManager.getChartUser(userCode));
	}

	/*
	 * 移除用户
	 */
	public static void removeChannelByCode(String userCode) {
		logger.info("移除用户:" + userCode);
		if(StringUtil.isNUll(userCode) || null==getChartUser(userCode)) {
			return;
		}
		
		/*
		 * Channel cl=ChannelManager.getChannel(userCode); Message message=new
		 * Message(); //线上禁言 message.setMsg("系统消息此用户已下线"); ChartUser admin=new
		 * ChartUser(); admin.setUserCode("admin"); admin.setUserName("管理员");
		 * message.setFrom(admin); message.setCmd("2");
		 * message.setCreateDate(StringUtil.format(new Date()));
		 * 
		 * ObjectMapper mapper = new ObjectMapper();
		 * 
		 * try { cl.writeAndFlush(new
		 * TextWebSocketFrame(mapper.writeValueAsString(message))); } catch
		 * (JsonProcessingException e) { e.printStackTrace(); }
		 */
		sendAlertMsgByCode(userCode, "系统消息此用户已下线");
		
		userChannel.remove(getChartUser(userCode));
		removeGroup(userCode);
	}

	/*
	 * 移除用户
	 */
	public static void removeChannelByChannel(Channel channel) {
		for (ChartUser key : userChannel.keySet()) {
			if (userChannel.get(key).equals(channel)) {
				logger.info("移除用户:" + key + "IP:" + channel.remoteAddress());
				removeChannelByCode(key.getUserCode());
				removeGroup(key.getUserCode());
				return;
			}
		}
	}

	//获取所有用户信息
	public static Set<ChartUser> getAllUser(){
		return userChannel.keySet();
	}
	
	/*
	 * 添加群组
	 */
	public static void addGroup(ChatGroup groupID, ChartUser cu) {
         if(null!=groupMap.get(groupID)) {
        	 groupMap.get(groupID).add(cu);
         }else {
        	 HashSet<ChartUser>set=new HashSet<ChartUser>();
        	 set.add(cu);
        	 groupMap.put(groupID, set);
         }
	}

	/**
	 * 移除群组
	 * @param groupID
	 * @param userCode
	 */
	public static void removeGroup(ChatGroup groupID,String userCode) {
		if(null!=groupMap.get(groupID)) {
			ChartUser cu=new ChartUser();
			cu.setUserCode(userCode);
			groupMap.get(groupID).remove(cu);
		}
	}
	
	/**
	 * 移除群组
	 * @param userCode
	 */
	public static void removeGroup(String userCode) {
		for(ChatGroup str:groupMap.keySet()) {
			ChartUser cu=new ChartUser();
			cu.setUserCode(userCode);
			groupMap.get(str).remove(cu);
		}
	}
	
	/**
	 * 查找是否存在
	 * @param groupID
	 * @param userCode
	 * @return
	 */
	public static boolean isExit(ChatGroup groupID,String userCode) {
		if(null!=groupMap.get(groupID)) {
			ChartUser cu=new ChartUser();
			cu.setUserCode(userCode);
			return groupMap.get(groupID).contains(cu);
		}else {
			return false;
		}
	}
	
	/**
	 * 获取群组用户
	 * @param groupID
	 * @return
	 */
	public static Set<ChartUser> getGroupUser(ChatGroup groupID){
		return groupMap.get(groupID);
	}
	
	
	/**
	 * 广播消息
	 * @param userCode
	 */
	public static void sendAlertMsgByCode(String userCode,String msg) {
		logger.info("广播用户:" + userCode);
		if(StringUtil.isNUll(userCode) || null==getChartUser(userCode)) {
			return;
		}
		
		Channel cl=ChannelManager.getChannel(userCode);
		Message message=new Message();
		message.setMsg(msg);
		ChartUser admin=new ChartUser();
		admin.setUserCode("admin");
		admin.setUserName("管理员");
		message.setFrom(admin);
		message.setCmd("2");
		message.setCreateDate(StringUtil.format(new Date()));
		
		ObjectMapper mapper = new ObjectMapper();
	 	
		try {
			cl.writeAndFlush(new TextWebSocketFrame(mapper.writeValueAsString(message)));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) {
		ChartUser cu1=new ChartUser();
		cu1.setUserCode("001");
		
		ChartUser cu2=new ChartUser();
		cu2.setUserCode("001");
		
		ChartUser cu3=new ChartUser();
		cu3.setUserCode("001");
		
		ChatGroup cg=new ChatGroup();
		cg.setGroupCode("G001");
		ChannelManager.addGroup(cg, cu1);
		ChannelManager.addGroup(cg, cu2);
		ChannelManager.addGroup(cg, cu3);
		
		System.out.println(ChannelManager.getGroupUser(cg).size());
	}
}
