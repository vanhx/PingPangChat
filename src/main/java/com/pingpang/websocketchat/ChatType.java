package com.pingpang.websocketchat;

public class ChatType {

	//绑定上线
	public static final String BIND="1";
	
	//下线
	public static final String LEAVE="2";
	
	//单聊
	public static final String SINGLE="3";
	
	//群聊
	public static final String GROUP="4";
	
	//获取用户信息
	public static final String QUERY_USER="5";
	
	//获取群组信息
	public static final String QUERY_GROUP="6";
	
	//进群
	public static final String ADD_GROUP="7";
	
	//离开
	public static final String REMORE_GROUP="8";
}
