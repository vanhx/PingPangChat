package com.pingpang.websocketchat;

import java.io.Serializable;
import java.util.Objects;

import com.pingpang.util.StringUtil;

public class ChatGroup implements Serializable{

	private static final long serialVersionUID = 7150107177297633010L;

	// 群组ID
	private String id;

	// 群组编码
	private String groupCode;

	// 群组编码
	private String groupName;

	// 群组备注
	private String groupPurpose;

	// 创建用户ID
	private String groupUserID;

	// 创建用户编码
	private String groupUserCode;

	// 群组状态0正常 -1删除
	private String groupStatus;

	//群组头像
	private String groupIMagePath;
	
	// 创建时间
	private String groupCreateDate;

	//统计当前群组的个数
	private int groupUserCount;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupPurpose() {
		return groupPurpose;
	}

	public void setGroupPurpose(String groupPurpose) {
		this.groupPurpose = groupPurpose;
	}

	public String getGroupUserID() {
		return groupUserID;
	}

	public void setGroupUserID(String groupUserID) {
		this.groupUserID = groupUserID;
	}

	public String getGroupUserCode() {
		return groupUserCode;
	}

	public void setGroupUserCode(String groupUserCode) {
		this.groupUserCode = groupUserCode;
	}

	public String getGroupStatus() {
		return groupStatus;
	}

	public void setGroupStatus(String groupStatus) {
		this.groupStatus = groupStatus;
	}

	public String getGroupCreateDate() {
		return groupCreateDate;
	}

	public void setGroupCreateDate(String groupCreateDate) {
		this.groupCreateDate = groupCreateDate;
	}
	
	public String getGroupIMagePath() {
		return groupIMagePath;
	}

	public void setGroupIMagePath(String groupIMagePath) {
		this.groupIMagePath = groupIMagePath;
	}
	
	public int getGroupUserCount() {
		return groupUserCount;
	}

	public void setGroupUserCount(int groupUserCount) {
		this.groupUserCount = groupUserCount;
	}
	
	@Override
	public String toString() {
		return "ChatGroup [id=" + id + ", groupCode=" + groupCode + ", groupName=" + groupName + ", groupPurpose="
				+ groupPurpose + ", groupUserID=" + groupUserID + ", groupUserCode=" + groupUserCode + ", groupStatus="
				+ groupStatus + ", groupIMagePath=" + groupIMagePath + ", groupCreateDate=" + groupCreateDate
				+ ", groupUserCount=" + groupUserCount + "]";
	}

	@Override
	public boolean equals(Object obj) {
		ChatGroup cg=(ChatGroup)obj;
		if(null!=cg && (this==cg || 
				        (!StringUtil.isNUll(this.getGroupCode()) && this.getGroupCode().equals(cg.getGroupCode()) && 
				        (StringUtil.isNUll(this.getGroupUserCode()) || this.getGroupUserCode().equals(cg.getGroupUserCode()))) ||
				        this.getId().equals(cg.getId()))) {
			return true;
		}else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.groupCode);
	}
	
}
