package com.pingpang.service;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pingpang.dao.UserDao;
import com.pingpang.redis.RedisPre;
import com.pingpang.redis.service.RedisService;
import com.pingpang.util.StringUtil;
import com.pingpang.websocketchat.ChartUser;

@Service
@Transactional
public class UserService {
   
   @Autowired
   private UserDao userDao;
   
   /**
    * redis
    */
   @Autowired
   private RedisService redisService;
   
    /**
	  * 添加用户
	 * @param user
	 */
	public Map<String,Object> addUser(ChartUser user) {
		ChartUser cu=new ChartUser();
		cu.setUserCode(user.getUserCode());
		cu=this.getUser(cu);
		if(null!=cu) {
			return StringUtil.returnMap("F", "用户编码已存在!");
		}
		
		user.setUserStatus("0");
	    user.setUserPassword(StringUtil.toMD5(user.getUserPassword()+user.getUserCode()));
		userDao.addUser(user);
		
		//这里重新获取放到redis中
		cu=userDao.getRedisUser(user);
		redisService.set(RedisPre.DB_USER+cu.getUserCode(), cu);
		
		return StringUtil.returnSucess();
	}
	
	
	/**
	  * 修改用户
	 * @param user
	 */
	public void updateUser(ChartUser user) {
		userDao.updateUser(user);
	}
	
  
	/**
	  *    获取用户
	 * @param user
	 * @return
	 */
	public ChartUser getUser(ChartUser user) {

		if(!StringUtil.isNUll(user.getUserPassword())){
			user.setUserPassword(StringUtil.toMD5(user.getUserPassword()+user.getUserCode()));
		}
		
		ChartUser cu=(ChartUser)redisService.get(RedisPre.DB_USER+user.getUserCode());

		if(null==cu) {
			//获取数据库内容,添加缓存数据
			cu=userDao.getRedisUser(user);
			
			if(null==cu) {
				return null;
			}
			
			redisService.set(RedisPre.DB_USER+user.getUserCode(),cu);
			
			cu.setUserPassword("");
			return cu;
			//return userDao.getUser(user);
		}else {
			if(!StringUtil.isNUll(user.getUserPassword())){
				if(cu.getUserPassword().equals(user.getUserPassword())){
					return cu;
				}else {
					return null;
				}
			}else {
				return cu;
			}
		}
	}
	
	
	/**
	 * 从redis获取用户的数量
	 * @return
	 */
	public int getAllRedisUser() {
		return redisService.getCountPrex(RedisPre.DB_USER+"*");
	}
	
	/**
	  *   获取用户总数
	 * @param queryMap
	 * @return
	 */
	public int getAllUserCount(Map<String,String> queryMap) {
		return userDao.getAllUserCount(queryMap);
	}
	
	/**
	   * 获取所有用户数据
	 * @param searchMap
	 * @return
	 */
	public Set<ChartUser> getAllUser(Map<String,String> searchMap){
		return userDao.getAllUser(searchMap);
	}
	
	/**
	   * 获取带密码的数据
	 * @param searchMap
	 * @return
	 */
	public Set<ChartUser>getRedisAllUser(Map<String,String>searchMap){
		return userDao.getRedisAllUser(searchMap);
	}
	//-------------非DAO数据，逻辑处理 开始--------------------------------------------
	
	/**
	   *   数据状状态同步
	 * @param userCode
	 * @param userStatus
	 */
	public void dbDownUser(String userCode,String userStatus){
		if(StringUtil.isNUll(userCode)){
			return;
		}
		
		Set<String> userCodes=new HashSet<String>();
		userCodes.add(userCode);
		dbDownUser(userCodes, userStatus);
	}
	
	/**
	   *   这里没用in操作，后续自行修改
	 * @param userCode
	 * @param userStatus
	 */
	public void dbDownUser(Set<String> userCode,String userStatus){
          if(null!=userCode && userCode.size()>0) {
        	  for(String str:userCode) {
        		  /*ChartUser cu=new ChartUser();
                  cu.setUserCode(str);
                  cu=userDao.getUser(cu);
                  cu.setUserStatus(userStatus);
                  */
        		  
        		  //从redis获取数据
        		  ChartUser cu=(ChartUser) redisService.get(RedisPre.DB_USER+str);
        		  cu.setUserStatus(userStatus);
                  //数据库更新
        		  userDao.updateUser(cu);
        		  //redis更新
        		  redisService.set(RedisPre.DB_USER+str, cu);
                  cu=null;
        	  }
          } 		
	}
	//-------------非DAO数据，逻辑处理 结束---------------------------------------------
	
	/**
	   * 获取用户注册数据
	 * @param day
	 * @return
	 */
	public List<Map<String,String>> getUserRegsitCount(int day){
		return userDao.getUserRegsitCount(day);
	}
}
