package com.pingpang.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pingpang.dao.UserMsgDao;
import com.pingpang.websocketchat.ChartUser;
import com.pingpang.websocketchat.Message;

@Service
@Transactional
public class UserMsgService {

	@Autowired
	private UserMsgDao userMsgDao;
	
	/**
	   *  添加信息
	 * @param msg
	 */
	public void addMsg(Message msg) {
		userMsgDao.addMsg(msg);
	}
	
	
	/**
	   *    获取总数
	 * @param queryMap
	 * @return
	 */
	public int getAllMsgCount(Map<String,String> queryMap) {
		return userMsgDao.getAllMsgCount(queryMap);
	}
	
	/**
	   * 获取所有信息
	 * @param queryMap
	 * @return
	 */
	public Set<Message> getAllMsg(Map<String,String> queryMap){
		Set<Message> msgSet=userMsgDao.getAllMsg(queryMap);
		/*
		 * for(Message msg:msgSet) { String fromIP=msg.getFrom().getIp(); String
		 * acceptIP=msg.getAccept().getIp();
		 * msg.setFrom(userDao.getUser(msg.getFrom()));
		 * msg.setAccept(userDao.getUser(msg.getAccept())); msg.getFrom().setIp(fromIP);
		 * msg.getAccept().setIp(acceptIP); }
		 */
		return msgSet;
	}
	
	
	/**
	   *  更新数据
	   *  状态0未发送 1已发送 -1删除
	 * @param id
	 */
	public void upMsg(Map<String,Object> queryMap) {
		userMsgDao.upMsg(queryMap.get("status").toString(),(String[])queryMap.get("id"));
	}
	
	/**
	  * 获取最近聊天得用户
	 * @param cu
	 * @return
	 */
	public List<Map<String,String>> getUserOldChat(ChartUser cu){
		return userMsgDao.getUserOldChat(cu);
	}
	

	/**
	 * 获取聊天信息记录
	 * @param currentCu
	 * @param otherUser
	 * @return
	 */
	public List<Message> getUserOldMsg(Message messge){
		return userMsgDao.getUserOldMsg(messge);
	}
	
	
	/**
	 * 根据用户离线信息获取
	 * @param cu
	 * @return
	 */
    public List<Message> getUserOutMsg(ChartUser cu){
    	return userMsgDao.getUserOutMsg(cu);
    }
    
    /**
     * 更新消息
     * status
     * id
     * 接收的IP
     * @param updateMap
     */
    public void upOutMsg(Map<String,Object> updateMap) {
    	userMsgDao.upOutMsg(updateMap);
    }
}
