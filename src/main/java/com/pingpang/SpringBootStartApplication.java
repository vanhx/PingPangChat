package com.pingpang;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class SpringBootStartApplication extends SpringBootServletInitializer{

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		 // 注意这里要指向原先用main方法执行的App启动类
		 return builder.sources(DemoApplication.class);
	}
}
