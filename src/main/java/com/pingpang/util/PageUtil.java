package com.pingpang.util;

import java.util.HashMap;
import java.util.Map;

public class PageUtil {

	/**
	 * 反回page 当前页码  默认1
	 * 返回limit 分页条数 默认10 
	 * 返回start 开始行号
	 * 返回 end   结束行号
	 * @param page  当前页
	 * @param limit 页大小
	 * @return
	 */
	public static Map<String,String> getPage(String page,String limit){
		Map<String,String> result=new HashMap<String,String>();
		if(StringUtil.isNUll(page) || !StringUtil.checkNum(page) ||Integer.valueOf(page)<=0) {
			page="1";
		}
		
		if(StringUtil.isNUll(limit) || !StringUtil.checkNum(limit) ||Integer.valueOf(limit)<=0) {
			limit="10";
		}
		
		int start=(Integer.valueOf(page)-1)*Integer.valueOf(limit)+1;
		int end=start+Integer.valueOf(limit)-1;
		
		result.put("page", String.valueOf(page));
		result.put("limit", String.valueOf(limit));
		result.put("start",String.valueOf(start));
		result.put("end", String.valueOf(end));
		return result;
	}
}
