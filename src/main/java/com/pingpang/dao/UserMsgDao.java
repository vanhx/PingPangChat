package com.pingpang.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.pingpang.websocketchat.ChartUser;
import com.pingpang.websocketchat.Message;

@Mapper
public interface UserMsgDao {

	/**
	   *  添加信息
	 * @param msg
	 */
	public void addMsg(Message msg);
	
	/**
	   *    获取总数
	 * @param queryMap
	 * @return
	 */
	public int getAllMsgCount(Map<String,String> queryMap);
	
	/**
	   * 获取所有信息
	 * @param queryMap
	 * @return
	 */
	public Set<Message> getAllMsg(Map<String,String> queryMap);
	
	
	/**
	   *  更新数据
	   *  状态0未发送 1已发送 -1删除
	 * @param id
	 */
	public void upMsg(@Param("status")String status,@Param("ids")String[] ids);
	
	
	/**
	  * 获取最近聊天得用户
	 * @param cu
	 * @return
	 */
	public List<Map<String,String>> getUserOldChat(ChartUser cu);
	
	/**
	 * 获取聊天信息记录
	 * @param currentCu
	 * @param otherUser
	 * @return
	 */
	public List<Message> getUserOldMsg(Message messge);
	
	
	/**
	 * 根据用户离线信息获取
	 * @param cu
	 * @return
	 */
    public List<Message> getUserOutMsg(ChartUser cu);
    
    /**
     * 更新消息
     * status
     * id
     * 接收的IP
     * @param updateMap
     */
    public void upOutMsg(Map<String,Object> updateMap);
    
    
}
