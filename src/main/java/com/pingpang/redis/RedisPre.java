package com.pingpang.redis;

public class RedisPre {

	/**
	 * redis中用户信息前缀 
	 */
	public final static String DB_USER="DB_USER_";
	
	/**
	 * redis群组信息前缀
	 */
	public final static String DB_GROUP="DB_GROUP_";
	
	/**
	 * redis群组成员前置
	 */
	public final static String DB_GROUP_SET="DB_GROUP_SET_";
}
