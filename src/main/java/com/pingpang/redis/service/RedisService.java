package com.pingpang.redis.service;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import com.pingpang.util.JsonFilterUtil;

@Service
public class RedisService {
    @Resource
    private RedisTemplate<String, Object> redisTemplate;
    
    @Resource
    private RedisTemplate<String, Object>redisTemplateGroup;
    
    /**
     * 添加数据
     * @param key
     * @param value
     */
    public void set(String key, Object value) {
    	delete(key);
        ValueOperations<String, Object> vo =  redisTemplate.opsForValue();
        vo.set(key, value);
    }

    /**
     * 获取数据
     * @param key
     * @return
     */
    public Object get(String key) {
        ValueOperations<String, Object> vo =  redisTemplate.opsForValue();
        return vo.get(key);
    }
    
    /**
     * 删除数据
     * @param key
     * @return
     */
    public boolean delete(String key) {
    	return redisTemplate.delete(key);
    }
    
    
    /**
     * 删除数据匹配前缀
     * @param prex
     * @return
     */
    public void deletePrex(String prex) {
    	Set<String> keys = redisTemplate.keys(prex);
        if (null!=keys && !keys.isEmpty()) {
            redisTemplate.delete(keys);
        }
    }
    
    /**
     * 获取key值
     * @param prex
     * @return
     */
    public Set<String> getSetPrex(String prex) {
    	return redisTemplate.keys(prex);
    }
    
    /**
     * 判断key是否存在
     * @param key
     * @return
     */
    public boolean isExitKey(String key) {
    	return redisTemplate.hasKey(key);
    }
    
    
    /**
     * 统计数量
     * @param prex
     * @return
     */
    public int getCountPrex(String prex) {
    	return redisTemplate.keys(prex).size();
    }
    
    /**
     * 获取set数据
     * @param key
     * @return
     */
    public Set<Object> getSet(String key){
    	return redisTemplateGroup.opsForSet().members(key);
    }
    
    /**
     * 单条set添加
     * @param key
     * @param ojbSet
     */
    public void addSet(String key,Object objSet) {
    	redisTemplateGroup.opsForSet().add(key,objSet);
    }
    
    /**
     *   添加set数据
     * @param key
     * @param ojbSet
     */
    public void addSet(String key,Set<Object> objSet) {
    	redisTemplateGroup.opsForSet().add(key, objSet);
    }
    
    /**
     * 判断是否包含某个元素
     * @param key
     * @param value
     */
    public boolean contains(String key, Object value) {
        return redisTemplateGroup.opsForSet().isMember(key, value);
    }
    
    /**
     * 移除set数据
     * @param key
     * @param value
     */
    public void removeSet(String key,Object value) {
    	redisTemplateGroup.opsForSet().remove(key, value);
    }
}
